package alphacivsrc.domain;

import java.util.ArrayList;

public interface WinStrategy {
	public Player checkForWinner(int age, ArrayList<CityImpl> cities, Player playerInTurn); 
}
