package alphacivsrc.domain;

public class GameLogImpl implements GameLog {
	private String results;
	
	public GameLogImpl() {
		results = new String();
	}
	
	@Override
	public void moveUnitLog(Position from, Position to, UnitImpl unit) {
		results += unit.getOwner() + " moves " + unit.getTypeString() 
		+ " from " + from.toString() + " to " + to.toString() + ".\n";
	}

	@Override
	public void changeProductionInCityAt(TileImpl tile, String unitType) {
		results += tile.getCity().getOwner() + " changes production in city at "
				+ tile.getPosition().toString() + " to " + unitType + ".\n";
	}

	@Override
	public void changeWorkForceFocusInCityAtLog(TileImpl tile, String balance) {
		results += tile.getCity().getOwner() + " changes work force focus in city at " +
				tile.getPosition().toString() + " to " + balance + ".\n";
	}

	@Override
	public void endOfTurnLog(Player playerInTurn) {
		results += playerInTurn + " ends turn.\n";

	}

	@Override
	public void performUnitActionAt(TileImpl tile) {
		UnitImpl unit = tile.getUnit();
		results += unit.getOwner() + " uses " + unit.getTypeString() + " at " +
				tile.getPosition();
	}

	@Override
	public String getLog() {
		return results;
	}
	
	public void printLog() {
		System.out.println(results);
	}

}
