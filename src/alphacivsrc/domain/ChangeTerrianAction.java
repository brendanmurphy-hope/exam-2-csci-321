package alphacivsrc.domain;

public class ChangeTerrianAction implements UnitActions{

	@Override
	public void unitPreformsAction(UnitContext unitContext) {
		String currentType = unitContext.getTile().getTypeString();
		TileImpl tile = unitContext.getTile();
		if(currentType.equals(GameConstants.FOREST)) {
			tile.setTypeString(GameConstants.PLAINS);
		}
		//do nothing else
	}

}
