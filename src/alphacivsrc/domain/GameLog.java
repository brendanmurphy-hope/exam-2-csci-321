package alphacivsrc.domain;

public interface GameLog {
	public void moveUnitLog( Position from, Position to, UnitImpl unit );
	
	public void changeProductionInCityAt( TileImpl tile, String unitType );
	
	public void changeWorkForceFocusInCityAtLog( TileImpl tile, String balance );
	
	public void endOfTurnLog(Player playerInTurn);
	
	public void performUnitActionAt( TileImpl tile);
	
	public String getLog();
}
