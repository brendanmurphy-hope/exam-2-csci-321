package alphacivsrc.domain;

public class ThirdPartyFractalGeneratorAdapter {
	private ThirdPartyFractalGenerator generator;
	
	public ThirdPartyFractalGeneratorAdapter() {
		generator = new ThirdPartyFractalGenerator() {
			
			@Override
			public char getLandscapeAt(int row, int col) {
				// TODO Auto-generated method stub
				return 0;
			}
		};
	}
	
	public TileImpl adaptTheTile(int row, int column) {
		Position pos = new Position(row, column);
		char type = generator.getLandscapeAt(row, column);
		TileImpl returnTile = new TileImpl(pos, null);
		if(type == '.') {
			returnTile = new TileImpl(pos, GameConstants.OCEANS);
		}else if (type == 'o') {
			returnTile = new TileImpl(pos, GameConstants.PLAINS);
		}else if (type == 'f') {
			returnTile = new TileImpl(pos, GameConstants.FOREST);
		}else if (type == 'h') {
			returnTile = new TileImpl(pos, GameConstants.HILLS);
		}else if (type == 'm') {
			returnTile = new TileImpl(pos, GameConstants.HILLS);
		}	
		return returnTile;
	}
}
