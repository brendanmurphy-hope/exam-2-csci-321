package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestGameLog {
	private GameLogImpl gameLog;

	/** Fixture for alphaciv testing. */
	@Before
	public void setUp() {
		gameLog = new GameLogImpl();
	}


	@Test
	public void testMovementLog() {
		UnitImpl unit = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		Position from = new Position(2, 0);
		Position to = new Position(3,1);
		gameLog.moveUnitLog(from, to, unit);
		assertEquals("RED moves archer from (2,0) to (3,1).\n", gameLog.getLog());
	}
	
	@Test
	public void testChangeProduction() {
		Position pos = new Position(1,1);
		TileImpl tile = new TileImpl(pos, GameConstants.PLAINS);
		CityImpl city = new CityImpl(Player.RED, pos);
		tile.setCity(city);
		gameLog.changeProductionInCityAt(tile, GameConstants.LEGION);
		assertEquals("RED changes production in city at (1,1) to legion.\n", gameLog.getLog());
	}
	
	@Test
	public void testEndTurn() {
		gameLog.endOfTurnLog(Player.RED);
		assertEquals("RED ends turn.\n", gameLog.getLog());
	}
	
	@Test 
	public void testChangeWorkForceFocus() {
		Position pos = new Position(4, 1);
		TileImpl tile = new TileImpl(pos, GameConstants.PLAINS);
		CityImpl city = new CityImpl(Player.BLUE, pos);
		tile.setCity(city);
		gameLog.changeWorkForceFocusInCityAtLog(tile, GameConstants.foodFocus);
		
		assertEquals("BLUE changes work force focus in city at (4,1) to food.\n", gameLog.getLog());
	}
	
	@Test
	public void testUnitAction() {
		Position pos = new Position(4, 1);
		TileImpl tile = new TileImpl(pos, GameConstants.PLAINS);
		UnitImpl unit = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		tile.setUnit(unit);
		gameLog.performUnitActionAt(tile);
		assertEquals("RED uses archer at (4,1)", gameLog.getLog());
	}

}
