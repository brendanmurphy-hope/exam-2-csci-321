package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestChangeTerrian {
	private UnitImpl settler;
	private GameImpl game;
	private TileImpl[][] boardArray;
	
	@Before
	public void setUp() throws Exception {
		settler = new UnitImpl(Player.RED, GameConstants.SETTLER, new SettlerAction());
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationAlphaCiv());
		boardArray = ((GameImpl) game).getBoardArray();
	}

	@Test
	public void testUnitChangingTerrian() {
		Position pos = new Position (1,1);
		TileImpl forest = new TileImpl(pos, GameConstants.FOREST);
		forest.setUnit(settler);
		UnitContext uc = new UnitContext(forest);
		settler.setUnitAction(new ChangeTerrianAction());
		boardArray[1][1] = forest;
		game.performUnitActionAt(pos);
		assertEquals("plains", boardArray[1][1].getTypeString());
	}

}
