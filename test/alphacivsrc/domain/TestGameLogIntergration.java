package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestGameLogIntergration {

	private GameImpl game;
	private TileImpl[][] boardArray;

	/** Fixture for alphaciv testing. */
	@Before
	public void setUp() {
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationAlphaCiv());

		boardArray = game.getBoardArray();
	}

	@Test
	public void testMultipleOutPutsAtOnce() {
		Position pos = new Position(1,1);
		TileImpl tile = new TileImpl(pos, GameConstants.PLAINS);
		CityImpl city = new CityImpl(Player.RED, pos);
		tile.setCity(city);
		GameLog gameLog = game.getGameLog();
		gameLog.changeProductionInCityAt(tile, GameConstants.LEGION);
		gameLog.endOfTurnLog(Player.RED);
		
		assertEquals("RED changes production in city at (1,1) to legion.\nRED ends turn.\n", gameLog.getLog());
		
	}

}
