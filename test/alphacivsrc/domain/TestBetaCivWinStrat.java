package alphacivsrc.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class TestBetaCivWinStrat {
	private BetaCivWinStrat winStrat;

	@Before
	public void setUp(){
		winStrat = new BetaCivWinStrat();
	}

	@Test
	public void testThatBlueWins() {
		ArrayList<CityImpl> cities = new ArrayList<CityImpl>();
		cities.add(new CityImpl(Player.BLUE, new Position(1, 1)));
		assertEquals(Player.BLUE, winStrat.checkForWinner(4000, cities, Player.BLUE));
	}
	
	@Test
	public void testThatRedWins() {
		ArrayList<CityImpl> cities = new ArrayList<CityImpl>();
		cities.add(new CityImpl(Player.RED, new Position(1, 1)));
		assertEquals(Player.RED, winStrat.checkForWinner(4000, cities, Player.RED));
	}

}
